
// returns the selected text from the document
function getSelectedText() {
  return String((document.all) ?
    document.selection.createRange().text :
    document.getSelection());
}

(function($) {
  Drupal.CTools = Drupal.CTools || {};
  Drupal.CTools.AJAX = Drupal.CTools.AJAX || {};
  Drupal.CTools.AJAX.commands = Drupal.CTools.AJAX.commands || {};

  // creates a HTML wrapper around the tag
  // to display it in a fancy way
  function createTagHTML(tag) {
    var html =
      $('<div class="click2tag-tag"><span></span><a href="#">x</a></div>');
    $('span', html).text(tag);
    return html;
  }

  // define ctools command
  Drupal.CTools.AJAX.commands.reinit_click2tag_form = function(data) {
    // applying CTools behavior
    Drupal.behaviors.CToolsAJAX(document);
    $('form', $(data.selector))
    .submit(function(event) {
      event.preventDefault();
      // copying values from the fancy display to the text input
      var terms = [];
      $('form div.termwidgets span', $(data.selector)).each(function(index, value) {
        var text = $(value).text();
        if(text != '') {
          text = text.replace(',', '');
          terms[terms.length] = text;
        }
      });
      $('form input[name=new_terms]', $(data.selector)).val(terms.join(', '));

      // preparing
      var formdata = $(this).serialize();
      formdata += '&op=' + escape($('input[type=submit]', $(this)).val());
      // sending
      $.ajax({
        type: 'post',
        url: $(this).attr('action'),
        data: formdata,
        success: function(response) {
          Drupal.CTools.AJAX.respond(response);
        },
        dataType: 'json',
      });
      return false;
    });

    // override the 'Add' button to add terms
    // to the form, instead of submitting it
    $('#edit-add', $('form', $(data.selector)))
    .click(function(event) {
      event.preventDefault();
      var text = getSelectedText();
      if(text == '') {
        return false;
      }
      var tag = createTagHTML(text);
      $('form div.termwidgets', $(data.selector)).append(tag);
      $('a', tag).click(function(event) {
        event.preventDefault();
        $(this).parent().remove();
        return false;
      });
      return false;
    });
  }

  $(function() {
    // initializing click2tag container
    $('div.click2tag-container')
    .not('.processed')
    .addClass('processed')
    .each(function() {
      // autoload forms for every vocabulary
      var classes = $(this).attr('class').split(' ');
      var node = 0;
      var vocabulary = 0;
      for(cls in classes) {
        cls = classes[cls];
        var node_candidate = cls.match(/node-([0-9]*)/);
        if(node_candidate != null) {
          node_candidate = node_candidate[1];
          if(node_candidate) {
            node = node_candidate;
          }
        }
        var vocabulary_candidate = cls.match(/vocabulary-([0-9]*)/);
        if(vocabulary_candidate != null) {
          vocabulary_candidate = vocabulary_candidate[1];
          if(vocabulary_candidate) {
            vocabulary = vocabulary_candidate;
          }
        }
      }
      if(node && vocabulary) {
        $.getJSON(Drupal.settings.basePath + '?q=click2tag/ajax/' + node + '/' + vocabulary, function(data) {
          Drupal.CTools.AJAX.respond(data);
        });
      }
    });
  });
})(jQuery);